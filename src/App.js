import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router, Switch, Route
} from "react-router-dom"
import jwt_decode from "jwt-decode"
import { URL } from "./config"


//./components
import Catalog from "./components/Catalog"
import Room from "./components/Room"
import Auth from "./components/Auth"
import Transactions from "./components/Transactions"
import UserTransactions from "./components/UserTransactions"
import ContactUs from "./components/ContactUs"

//./components/layouts
import TopNav from "./components/layouts/TopNav"

//./components/forms
import Login from "./components/forms/Login"
import Register from "./components/forms/Register"
import AddRoom from "./components/forms/AddRoom"

function App() {
  const [rooms, setRooms] = useState([])
  const [user, setUser] = useState({})
  const [token, setToken] = useState("")
  //
  const [categories, setCategories] = useState([])
  const [genres, setGenres] = useState([])

  useEffect (() => {
    fetch(`${URL}/categories`, {
      headers: {
        "x-auth-token": localStorage.getItem("token")
      }
    })
    .then(res => res.json())
    .then(data => {
      setCategories(data)
    })

  }, [])

  useEffect (() => {
    fetch(`${URL}/genres`, {
      headers: {
        "x-auth-token": localStorage.getItem("token")
      }
    })
    .then(res => res.json())
    .then(data => {
      setGenres(data)
    })

  }, [])

  //
  useEffect ( () => {
    fetch(`${URL}/rooms`)
    .then(res => res.json())
    .then(data => {
      setRooms(data)
    })

    //for expired token clearing
    if(token) {
      let decoded = jwt_decode(token)
      let now = new Date()
      if(decoded.exp === now.getTime()){
        localStorage.clear()
      }
    }
    setUser(JSON.parse(localStorage.getItem("user")))
    setToken(localStorage.getItem("token"))
  }, [token])

  const logoutHandler = () => {
    localStorage.clear()
    setUser({})
    setToken("")
    window.location.href="/"
  }


  return (
    <Router>
      <TopNav 
        user={user}
        token={token}
        logoutHandler={logoutHandler}
      />

      <Switch>

        <Route path="/register">
          <Register />
        </Route>

        <Route path="/login">
          <Login />
        </Route>

        <Route path="/add-room">
          {
            user && token && user.isAdmin ?
            <AddRoom 
              categories={categories}
            /> :
            <Auth />
          }
        </Route>

        <Route path="/rooms/:id">
          <Room 
            user={user} 
            token={token} 
            genres={genres} 
            categories={categories}
          />
        </Route>

        <Route exact path="/transactions/:userId">
          {
            user && token && user.isAdmin === false ?
            <UserTransactions /> :
            <Auth />
          }
        </Route>

        <Router path="/transactions">
          {
            user && token && user.isAdmin ?
            <Transactions /> :
            <Auth />
          }
        </Router>

        <Router path="/contact-us">
          <ContactUs />
        </Router>

        <Route exact path="/">
          <Catalog 
            rooms={rooms}
            user={user}
            token={token}
            categories={categories}
          />
        </Route>

      </Switch>

    </Router>
  )
}

export default App;
