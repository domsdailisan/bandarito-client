import React, { useState, useEffect, Fragment } from "react"
import { useParams } from "react-router-dom"
import { 
	Card, CardImg, Modal, ModalBody 
} from "reactstrap"
import Swalert from "sweetalert2"
import Calendar from 'react-calendar'
import DatePicker from 'react-date-picker'
import "./Room.css"

import EditRoom from "./forms/EditRoom"
import Footer from "./Footer"
import Stripe from "./forms/Stripe"
import { URL } from "../config"

const Room = ({user, token, categories}) => {
	let {id} = useParams()
	const [room, setRoom] = useState({})
	const [editing, setEditing] = useState(false)
	const [cartItems, setCartItems] = useState([])
	const [date, setDate] = useState(new Date())
	const minDate = new Date()

	const [modal, setModal] = useState(false)
	const toggle = () => setModal(!modal)

	useEffect (() => {
		fetch(`${URL}/rooms/${id}`)
		.then(res => res.json())
		.then(data => {
			setRoom(data)
		})
		// console.log("dito yun")
	}, [id])

	useEffect( () => {
		setCartItems(JSON.parse(localStorage.getItem('cartItems')))
	}, [])

	const deleteRoom = (roomId) => {
        Swalert.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            fetch(`${URL}/rooms/`+roomId, {
                method: "DELETE",
                headers: {
                    "x-auth-token": token
                }
            })
            .then(res => res.json())
            .then(data => {
                Swalert.fire(
                  'Deleted!',
                  `${data.message}`,
                  'success'
                )
                window.location.href="/"
            })
          }
        })
    }

    let total=0

    if(cartItems.length) {
    	cartItems.forEach(item => {
    		total += item.price * item.quantity
    	})
    }

  	const checkout = (e, room) => {
  		let cartItems = JSON.parse(localStorage.getItem('cartItems'))

  		let matched = cartItems.find(item => {
  			return item._id === room._id
  		})
  		
  		if (matched) {
  			matched.sched = sched
  		}

  		localStorage.setItem('cartItems', JSON.stringify(cartItems))
  		// console.log(cartItems)
  		let orders = JSON.parse(localStorage.getItem('cartItems'))
  		fetch(`${URL}/transactions`, {
  			method: "POST",
  			body: JSON.stringify({orders, total, sched}),
  			headers: {
  				"Content-Type": "application/json",
  				"x-auth-token": localStorage.getItem('token')
  			}

  		})
  		// console.log(orders)
  		.then(res => res.json())
  		.then(data => {
  			// console.log(data)
  			Swalert.fire({
  				'icon': 'success',
  				'title': data.message
  			})
  			localStorage.setItem('cartItems', JSON.stringify([]))
  			setCartItems(JSON.parse(localStorage.getItem('cartItems')))
  		})
  		// window.location.href="/"
  	}  

  	const cancelCartItem = () => {
  		localStorage.setItem('cartItems', JSON.stringify([]))
		setCartItems(JSON.parse(localStorage.getItem('cartItems')))
		window.location.href="/"
  	}

  	const dateHandler = (date) => {
  		// console.log(date)
  		setDate(date)
  	}

  	const sched = date.toLocaleDateString()

	return (
		<div>
			
			<Card className="p-2 m-4">
				{
					editing ?
					<EditRoom room={room} setEditing={setEditing} categories={categories}/> :
					<div className="text-center">
						<CardImg onClick={toggle} src={`${URL}${room.image}`}/>
						<h3 className="room-name">{room.name}</h3>
						<p><strong>{room.description}</strong></p>
						<h6><em>Price: </em>{room.price}</h6>
						<h6><em>Category:</em> {
							categories.map(category => {
								return(
									category._id === room.category ?
									category.name:
									null
								)
							})
						}</h6>
						{
							user && user.isAdmin && token ?
							<div className="text-center">
								<button className="btn-lg btn-warning mx-2" onClick={() => setEditing(true)}>Edit</button>
								<button className="btn-lg btn-danger mx-2" onClick={() => deleteRoom(room._id)}>Delete</button>
							</div> :
							null
						}

						{ 
							user && token && user.isAdmin === false && cartItems.length ?
							<Fragment>
							<h6>
								<em>Schedule: </em>
								<strong>{date.toLocaleDateString()}</strong>
							</h6>
							<div className="d-flex justify-content-center m-2">
								<Calendar
									onChange={dateHandler}
									minDate={minDate}
									value={date}
								 />
							</div>

							<div className="d-flex justify-content-around">
								<button 
									className="btn btn-success"
									onClick={(e) => {checkout(e, room)}}>
									<strong>Confirm Booking via COD</strong>
								</button>
								
								<Stripe 
									amount={total*100} 
									cartItems={cartItems} 
									sched={sched}
									setCartItems={setCartItems}
								/>

								<button 
								className="btn btn-warning" 
								onClick={cancelCartItem}>
									<strong>Cancel Booking</strong>
								</button>

							</div> 
							</Fragment>:
							null
						}
					</div>
				}
			</Card>
			{
				user.isAdmin ?
				null :
				<Footer />
			}
			

			<Modal isOpen={modal} size="lg" toggle={toggle}>
				<ModalBody>
					<CardImg src={`${URL}${room.image}`}/>
				</ModalBody>
			</Modal>
		</div>
	)
}

export default Room