import React from "react"
import "./CatalogBlog.css"

const CatalogBlog = () => {
	return (
		<div className="text-center catalogblog">
			<h1>What we do?</h1>
			<div className="row">
			<div className="whatwedo-texts col-md-6">
				<h3>RECORDING</h3>
				<p>The acoustics in our <strong>LIVE ROOM</strong> are phenomenal for drums, grand piano, and string ensembles. We also have a great collection of some of the finest microphones in the world and there are keyboards, guitars, and amps to hand in all studios.</p>
			</div>
			<div className="whatwedo-texts col-md-6">
				<h3>MIXING / MASTERING</h3>
				<p>We offer mixing and mastering at Palma Music Studios. Anything from a classic analogue mix on the SSL-Duality through the ATC-monitors in <strong>THE QUINCY ROOM</strong> To the favourite choice studio among our urban/electronic music clients, THE MERCURY ROOM, with large monitors, for that magical low end! We also have a network of amazing professional mixers and mastering engineers around the world, who can achieve the exact mix and master you need.</p>
			</div>
			<div className="whatwedo-texts col-md-6">
				<h3>AUDIO POST /SOUND DESIGN</h3>
				<p>For surround mixing and post-production, the <strong>WINEHOUSE ROOM</strong> is the studio to work out of. It features a 5.1 Genelec system, Antelope or Universal Audio interfaces, a Mac Pro with ProTools HD, Izotope RX7, and most other needed software. Our engineers have mixed everything from feature films, orchestral albums to TV-shows and surround live concerts in this studio.</p>
			</div>
			<div className="whatwedo-texts col-md-6">
				<h3>VOICE RECORDING</h3>
				<p>Want to record voice over for a film, a commercial, audio book or podcast? We’ve done all of the above, including trailers for Hollywood films and political campaigns for the Balearic president.  <strong>MERCURY ROOM</strong> is our main studio for recording vocals, and it’s also perfect for voice-over recordings.</p>
			</div>
			</div>
		</div>
	)
}

export default CatalogBlog