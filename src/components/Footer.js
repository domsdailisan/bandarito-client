import React from "react"
import "./Footer.css"
import { Link } from "react-router-dom"

const Footer = () => {
	return (
		<div>
			<footer>
				<div className="footer">
					<div className="footer-content">
						<div className="footer-section about">
							<h1 className="logo-text"><span>BANDA</span>rito</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat.</p>

							<div className="contact">
								<span><i className="fas fa-phone">&nbsp; 123-456-789</i></span>
								<span><i className="fas fa-envelope">&nbsp; info@BANDArito.com</i></span>
							</div>

							<div className="socials">
								<Link to="/"><i className="fab fa-facebook"></i></Link>
								<Link to="/"><i className="fab fa-instagram"></i></Link>
								<Link to="/"><i className="fab fa-twitter"></i></Link>
								<Link to="/"><i className="fab fa-youtube"></i></Link>
							</div>
						</div>

						<div className="footer-section links">
							<h2>Quick Links</h2>
							<ul>
								<Link to="/"><li>Catalog</li></Link>
								<Link to="/login"><li>Login</li></Link>
								<Link to="/register"><li>Register</li></Link>
								<Link to="/contact-us"><li>Contact</li></Link>
							</ul>
						</div>

						<div className="footer-section contact-form">
							<h2>Contact Us</h2>
							<form action="index.html" method="post">
								<input type="email" name="email" className="text-input contact-input form-control" placeholder="Your email"/>
								<textarea className="text-input contact-input form-control mt-2" name="message" placeholder="Your message..."></textarea>
								<button type="submit" className="btn btn-big text-white">
									<i className="fas fa-envelope"></i> Send
								</button>	
							</form>
						</div>

					</div>
					<div className="footer-bottom">
						&copy; BANDArito | Designed by Doms
					</div>
				</div>
			</footer>
		</div>
	)
}

export default Footer