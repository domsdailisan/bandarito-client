import React, { useState, Fragment } from "react"
import {
	Navbar, NavbarToggler, Nav,
	Collapse, NavItem, Container
} from "reactstrap"
import { Link } from "react-router-dom"
import "./TopNav.css"
import { URL } from "../../config"

const TopNav = ({user, token, logoutHandler}) => {
	const [collapsed, setCollapsed] = useState(true)
	let guestLinks
	let authLinks

	if(!user && !token) {
		guestLinks=(
			<Fragment>
				<NavItem>
					<Link to="/login" className="nav-link">Login</Link>
				</NavItem>
				<NavItem>
					<Link to="/register" className="nav-link">Register</Link>
				</NavItem>
				<NavItem>
					<Link to="/contact-us" className="nav-link">Contact</Link>
				</NavItem>
			</Fragment>
		)
	}

	if(user && token) {
		authLinks = (
			<Fragment>
				<NavItem>
					<Link to="/" className="nav-link" onClick={logoutHandler}>Logout</Link>
				</NavItem>

				{
					user.isAdmin === false ?
					<Fragment>
						<NavItem>
							<Link to={`/transactions/${user._id}`} className="nav-link">Transactions</Link>
						</NavItem>
					</Fragment> : null
				}
				
				{
					user.isAdmin ? 
					<Fragment>
						<NavItem>
							<Link to="/add-room" className="nav-link">Add Room</Link>
						</NavItem> 
						<NavItem>
							<Link to="/transactions" className="nav-link">Transactions</Link>
						</NavItem> 
					</Fragment>:
					null
				}
				
			</Fragment>
		)
	}

	return (
		
			<div className="topnavbg container-fluid p-0">
				<Navbar color="dark" dark expand="md" className="TopNavNavbar">
					<Link to="/" className="navbar-brand">
						<img src={`${URL}/rooms/BANDArito_logo.png`} 
							className="LogoBrand" 
							alt="logo" 
						/>
					</Link>
					<NavbarToggler onClick={() => setCollapsed(!collapsed)}/>
					<Collapse isOpen={!collapsed} navbar>
						<Nav navbar className="ml-auto text-center TopNavText">
							<NavItem>
								<Link to="/" className="nav-link">Catalog</Link>
							</NavItem>
							{guestLinks}
							{authLinks}
						</Nav>
					</Collapse>
				</Navbar>
			</div>
		
	)
}

export default TopNav