import React, { useState } from "react"
import Swalert from "sweetalert2"
import { URL } from "../../config"
import "./Register.css"


const Register = () => {
	const [formData, setFormData] = useState({})
	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value

		})

	}
	// console.log(formData)

	const onSubmitHandler = (e) => {
		e.preventDefault()
		fetch(`${URL}/users/register`, {
			method: "POST",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200 ){
				Swalert.fire({
					icon: 'success',
					text: data.message
				})
				window.location.href="/"
			}else {
				Swalert.fire({
					icon: 'error',
					text: data.message,
					timer: 2000,
					showConfirmButton: false,
					timerProgressBar: true
				})
			}
		})
	}

	return (
		<div className="registration-box">
			<div className="registration-form">
				<div>
					<img src={`${URL}/rooms/BANDArito_logo.png`} 
						className="LogoBrand" 
						alt="logo" 
					/>
				</div>
				<h1>Registration</h1>
				<form onSubmit={onSubmitHandler}>
					<div className="textbox form-group">
						<label htmlFor="username"><strong>Username</strong></label>
						<input 
							type="text" 
							placeholder="Username" 
							name="username" 
							id="username" 
							className="form-control"
							onChange={onChangeHandler}
						/>
					</div>

					<div className="textbox form-group">
						<label htmlFor="bandName"><strong>Band Name</strong></label>
						<input 
							type="text" 
							placeholder="Band Name" 
							name="bandName" 
							id="bandName" 
							className="form-control"
							onChange={onChangeHandler}
						/>
					</div>

					<div className="textbox form-group">
						<label htmlFor="password"><strong>Password</strong></label>
						<input 
							type="password" 
							placeholder="Password" 
							name="password" 
							id="password" 
							className="form-control"
							onChange={onChangeHandler}
						/>
					</div>

					<div className="textbox form-group">
						<label htmlFor="password2"><strong>Confirm Password</strong></label>
						<input 
							type="password" 
							placeholder="Password" 
							name="password2" 
							id="password2"
							className="form-control"
							onChange={onChangeHandler}
						/>
					</div>

					<div className="textbox form-group">
						<label htmlFor="email"><strong>Email</strong></label>
						<input 
							type="email" 
							placeholder="email" 
							name="email" 
							id="email" 
							className="form-control"
							onChange={onChangeHandler}
						/>
					</div>

					<div className="textbox form-group">
						<label htmlFor="contact"><strong>Mobile#</strong></label>
						<input 
							type="text" 
							placeholder="contact" 
							name="contact" 
							id="contact" 
							className="form-control"
							onChange={onChangeHandler}
						/>
					</div>

					<button className="btn btn-success">Register</button>
				</form>
			</div>
		</div>
	)
}

export default Register