import React, { useState } from "react"
import Swalert from "sweetalert2"
import { URL } from "../../config"
import "./Login.css"

const Login = () => {
	const [formData, setFormData] = useState({})

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}
	const onSubmitHandler = (e) => {
		e.preventDefault()
		
		fetch(`${URL}/users/login`, {
			method: "POST",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.auth) {
				Swalert.fire({
					icon: 'success',
					title: data.message,
					showConfirmButton: false,
					timer: 3500
				})
				localStorage.setItem("user", JSON.stringify(data.user))
				localStorage.setItem("token", data.token)
				let cartItems = []
				localStorage.setItem("cartItems", JSON.stringify(cartItems))
				window.location.href="/"
			}else {
				Swalert.fire({
					icon: 'error',
					title: data.message,
					showConfirmButton: false,
					timer: 2500,
					timerProgressBar: true
				})
			}
		})
	}

	return (
		

		<div className="login-box">
			<div className="login-form">
				<div>
					<img src={`${URL}/rooms/BANDArito_logo.png`}
						className="LogoBrand" 
						alt="logo" 
					/>
				</div>
				<h1>Login</h1>
				<form onSubmit={onSubmitHandler}>
					<div className="textbox form-group">
						<label htmlFor="username"><strong>Username</strong></label>
						<input 
							type="text" 
							placeholder="Username" 
							name="username" 
							id="username" 
							className="form-control"
							onChange={onChangeHandler}
						/>
					</div>

					<div className="textbox form-group">
						<label htmlFor="password"><strong>Password</strong></label>
						<input 
							type="password" 
							placeholder="Password" 
							name="password" 
							id="password"
							className="form-control"
							onChange={onChangeHandler}
						/>
					</div>

					<button className="btn btn-success">Login</button>
				</form>
			</div>
		</div>
	)
}

export default Login


// <form className="mx-auto col-sm-6" onSubmit={onSubmitHandler}>
// 	<div className="form-group">
// 		<label htmlFor="username">Username</label>
// 		<input type="text" className="form-control" name="username" id="username" onChange={onChangeHandler} />
// 	</div>
// 	<div className="form-group">
// 		<label htmlFor="password">Password</label>
// 		<input type="password" className="form-control" name="password" id="password" onChange={onChangeHandler} />
// 	</div>
// 	<button className="btn btn-success">Login</button>
// </form>