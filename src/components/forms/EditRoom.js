import React, { useState } from "react"
import Swalert from "sweetalert2"
import { URL } from "../../config"
import "./EditRoom.css"

const EditRoom = ({room, setEditing, categories}) => {
	const [formData, setFormData] = useState(room)

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

	const handleFile = (e) => {
		setFormData({
			...formData,
			image: e.target.files[0]
		})
	}

	const onSubmitHandler = (e) => {
		e.preventDefault()
		let updatedRoom = new FormData()
		updatedRoom.append('name', formData.name)
		updatedRoom.append('description', formData.description)
		updatedRoom.append('category', formData.category)
		updatedRoom.append('price', formData.price)
		if(typeof formData.image === 'object') {
			updatedRoom.append('image', formData.image)
		}

		fetch(`${URL}/rooms/${formData._id}`, {
			method: "PUT",
			body: updatedRoom,
			headers: {
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => {
			Swalert.fire({
				icon: 'success',
				'text': data.message
			})
			setEditing(false)
		})
	}

	return(
		<div className="edit-room container">
			<div className="col-8 mx-auto">
				<form onSubmit={onSubmitHandler} encType="multipart/form-data">
					<div className="form-group">
						<label htmlFor="name">Name</label>
						<input 
							type="text" 
							name="name" 
							id="name"
							className="form-control" 
							onChange={onChangeHandler} 
							value={formData.name}
						/>
					</div>
					<div className="form-group">
						<label htmlFor="description">Description</label>
						<textarea 
							type="text" 
							name="description" 
							id="description"
							className="form-control" 
							onChange={onChangeHandler} 
							value={formData.description}
						>
						</textarea>
					</div>
					<div className="form-group">
						<label htmlFor="price">Price</label>
						<input 
							type="number" 
							name="price" 
							id="price"
							className="form-control" 
							onChange={onChangeHandler} 
							value={formData.price}
						/>
					</div>
					<div className="form-group">
						<label>Image</label>
						<img src={`${URL}${formData.image}`} alt="img" style={{height: "40px"}} className="p-1"/>
						<input type="file" name="image" className="form-control" onChange={handleFile} />
					</div>
					<select className="form-control mb-3" name="category" onChange={onChangeHandler}>
						{categories.map(category => {
							return(
								category._id === formData.category ?
								<option value={category._id} selected>{category.name}</option> :
								<option value={category._id} key={category._id}>{category.name}</option>
							)
						})}
					</select>
					<div className="text-center">
						<button className="btn btn-primary mx-2">Edit Room</button>
						<button className="btn btn-warning mx-2" type="button" onClick={() => setEditing(false)}>Cancel</button>
					</div>
				</form>
			</div>
		</div>
	)
}

export default EditRoom