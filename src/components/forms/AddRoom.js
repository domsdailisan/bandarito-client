import React, { useState } from "react"
import Swalert from "sweetalert2"
import { URL } from "../../config"
import "./AddRoom.css"

const AddRoom = ({categories}) => {
	const [formData, setFormData] = useState({})

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

	const handleFile = (e) => {
		// console.log(e.target.files[0])
		setFormData({
			...formData,
			image: e.target.files[0]
		})
	}

	const onSubmitHandler = (e) => {
		e.preventDefault()
		const room = new FormData()
		room.append('name', formData.name)
		room.append('description', formData.description)
		room.append('category', formData.category)
		room.append('price', formData.price)
		room.append('image', formData.image)

		fetch(`${URL}/rooms`, {
			method: "POST",
			body: room,
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200) {
				Swalert.fire({
					icon: 'success',
					text: data.message
				})
				window.location.href="/"
			}else {
				Swalert.fire({
					icon: 'success',
					text: "Please check your inputs"
				})
			}
		})
	}

	return (
		<div className="add-room">
			<div className="col-8 mx-auto">
				<form onSubmit={onSubmitHandler} encType="multipart/form-data">
					<div className="form-group">
						<label htmlFor="name">Name</label>
						<input type="text" name="name" className="form-control" id="name" onChange={onChangeHandler}/>
					</div>
					<div className="form-group">
						<label htmlFor="description">Description</label>
						<textarea type="text" name="description" className="form-control" id="description" onChange={onChangeHandler}>
						</textarea>
					</div>
					<div className="form-group">
						<label htmlFor="price">Price</label>
						<input type="number" name="price" className="form-control" id="price" onChange={onChangeHandler}/>
					</div>
					<select className="form-control mb-3" name="category" onChange={onChangeHandler}>
						<option > Select Category </option>
						{categories.map(category => (
							<option value={category._id} key={category._id}>{category.name}</option>
						))}
					</select>
					<div className="form-group">
						<label>Image</label>
						<input type="file" name="image" className="form-control" onChange={handleFile}/>
					</div>
					<button className="btn btn-primary">Add Room</button>
				</form>
			</div>
		</div>
	)
}

export default AddRoom