import React, { useState, useEffect, Fragment } from "react"
import { useParams } from "react-router-dom"
import { URL } from "../config"
import "./UserTransactions.css"
import Footer from "./Footer"

const UserTransactions = () => {

	let {userId} = useParams()
	const [transactions, setTransactions] = useState([])

	useEffect( () => {
		fetch(`${URL}/transactions/${userId}`, {
			headers: {
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => setTransactions(data))
	}, [userId])
	// 
	return (
		<Fragment>
			<div className="transContainer">
				<div className="usertrans">
					<h1>My Transactions</h1>
					{
						transactions.length ? 
						<table className="table table-hover table-bordered table-striped table-dark">
							<thead>
								<tr>
									<th>Transaction ID</th>
									<th>Status</th>
									<th>Amount</th>
									<th>Schedule</th>
									<th>Date of Order</th>
								</tr>
							</thead>
							<tbody>
								{transactions.map(transaction =>(
									<tr key={transaction._id}>

										<td>{transaction._id}</td>
										<td>
											{
												transaction.statusId === "5e9d2e50e6e19814785be47d" ?
												"Pending" : (transaction.statusId === "5e9d2e9ae6e19814785be47e" ?
												"Completed" : "Cancelled"
												)
											}
										</td>
										<td>PHP {transaction.total}</td>
										<td>{transaction.sched}</td>
										<td>{new Date(transaction.dateCreated).toLocaleString()}</td>
									</tr>
								))}
							</tbody>
						</table> :
						<h2>No Transactions to show</h2>
					}
				</div>
			</div>
			<div className="container p-0">
			<Footer />
			</div>
		</Fragment>
	)
}

export default UserTransactions