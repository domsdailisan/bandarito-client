import React from "react"
import { Link } from "react-router-dom"
import "./Auth.css"

const Auth = () => {
	return (
		<div className="container auth">
			<h1>
				You have no access on this page please
			</h1>
			<h1 className="textLink">
				<Link to="/login"><strong>LOGIN</strong></Link>
			</h1>	
			<h3>or</h3>
			<h1>View our</h1>
			<h1 className="textLink">
				<Link to="/"><strong>CATALOG</strong></Link>
			</h1>
		</div>
	)
}

export default Auth;