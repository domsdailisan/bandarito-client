import React from "react"
import { 
	Container, Row, Col, Card, 
	CardTitle, CardText, CardImg
} from "reactstrap"
import { Link } from "react-router-dom"
import { URL } from "../config"
import {
	Carousel
} from "react-bootstrap"
import CatalogBlog from "./CatalogBlog"
import Footer from "./Footer"
import "./Catalog.css"

const Catalog = ({rooms, user, token, categories}) => {
	

	const bookNowHandler = (e, room) => {
		
		let cartItems = JSON.parse(localStorage.getItem('cartItems'))

		let matched = cartItems.find(item => {
			return item._id === room._id
		})
		
		if (matched) {
			matched.quantity = 1
		}else {
			cartItems.push({
				...room,
				quantity: 1
			})
		}
		localStorage.setItem('cartItems', JSON.stringify(cartItems))
		
		window.location.href=`/rooms/${room._id}`
		// return false
		// window.location.replace(`https://sad-easley-1e7499.netlify.app/rooms/${room._id}`)
	}

	const showRooms = rooms.map(room => (
		<Col lg="3" md="6" className="pt-2" key={room._id}>
			<Card className="p-2">
			<CardImg src={`${URL}`+room.image}/>
				{
					!user ?
					<CardTitle>
						<em>Name: </em>{room.name}
					</CardTitle>:
					<CardTitle>
						<em>Name: </em>
						<Link 
							className="cardtitlelink" 
							to={`/rooms/${room._id}`}>
								{room.name}
						</Link>
					</CardTitle>
				}
				<CardText><em>Description: </em>{room.description}</CardText>
				<h6><em>Price:</em> {room.price}</h6>
				<h6><em>Category:</em> {
					categories.map(category => {
						return(
							category._id === room.category ?
							category.name:
							null
						)
					})
				}</h6>
				
				{ 
					user && token && user.isAdmin === false?
					<div>
						
						<button 
							type="button"
							onClick={(e) => {
							bookNowHandler(e, room)
						}}>
							<strong>Book Now!</strong>
						</button>
						
					</div> :
					null
				}
				{
					!user ? 
					<h6 className="text-center">Register/Login to see more details!</h6>:
					null
				}
			</Card>
		</Col>
	))

	const showcase = (
		<Carousel className="carousel">
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src={`${URL}/rooms/1587518342237-standard_room.jpg`}
		      alt="First slide"
		    />
		    <Carousel.Caption className="carousel-caption">
		      <h3>QUINCY ROOM</h3>
		      <p>It has everything you would expect from a classic analog studio, combined with the latest technology and audio connections to all rooms in the building. A 48-channel Solid State Logic Duality desk as a centerpiece and ATC SCM150 speakers as main monitors.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src={`${URL}/rooms/1587518454545-deluxe_room.jpg`}
		      alt="Third slide"
		    />

		    <Carousel.Caption className="carousel-caption">
		      <h3>THE LIVE ROOM</h3>
		      <p>Live Room offers a fantastic ambience/acoustics and includes a selection of the finest microphones on the planet. We designed it to look like we had excavated an old wall from the nearby castle-like building of Pueblo Español. The two ceiling windows give a natural light, but can be covered with remote blinders. This room is a perfect choice for any live recordings. With a full instrument back line and PA system, it also makes the Live Room a perfect place for intimate show-cases.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src={`${URL}/rooms/1587518603725-king_room.jpg`}
		      alt="Third slide"
		    />

		    <Carousel.Caption className="carousel-caption">
		      <h3>THE MERCURY ROOM</h3>
		      <p>The Mercury Room is our main production studio on floor one. Built for the modern producer with an amazing monitor system and a variety of synths and beat maker tools</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src={`${URL}/rooms/1587518632962-suite_room.jpg`}
		      alt="Third slide"
		    />

		    <Carousel.Caption className="carousel-caption">
		      <h3>THE WINEHOUSE ROOM</h3>
		      <p>This is our surround suite and features a 5.1 monitor setup for post-production and offline, but can be used as recording, mixing and producing music as well. The room has windows into the Live Room balcony and is right next to the roof top terrace stairs and the kitchen/lounge.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>
	)



	return (
		<div className="catalog container-fluid p-0">
			<div className="showcase">
				{showcase}
			</div>
			<h2 className="text-center cardbanner">BAND REHEARSAL ROOMS</h2>
			<Row className="showrooms p-3">
				{showRooms}
			</Row>
			<CatalogBlog />
			<Footer />
		</div>
	)
}

export default Catalog
