import React, { useState, useEffect, Fragment } from "react"
import { URL } from "../config"
import "./Transactions.css"
import Swalert from "sweetalert2"

const Transactions = () => {
	const [transactions, setTransactions] = useState([])
	// const [status, setStatus] = useState("")

	useEffect( () => {
		fetch(`${URL}/transactions`, {
			headers: {
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => setTransactions(data))
	})

	// useEffect( () => {
	// 	fetch(`http://localhost:4000/statuses`)
	// 	.then(res => res.json())
	// 	.then(data => setStatus(data))
	// })

	const updateStatus = (statusId, transactionId) => {
		let body = {
			statusId
		}
		// console.log(statudId)
		// console.log(transactionId)

		fetch(`${URL}/transactions/${transactionId}`, {
			method: "PUT",
			body: JSON.stringify(body),
			headers: {
				"Content-Type": "application/json",
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => {
			// alert(data.message)
			Swalert.fire({
				icon: 'success',
				title: data.message,
				showConfirmButton: false,
				timer: 1000
			})
		})
	}

	return (
		<Fragment>
			<div className="transContainer">
				<div className="usertrans">
					<h1>Transactions</h1>
					{
						transactions.length ?
						<table className="table table-hover table-bordered table-striped table-dark">
							<thead>
								<tr>
									<th>Transaction ID</th>
									<th>Status</th>
									<th>Amount</th>
									<th>Schedule</th>
									<th>Action/s</th>
								</tr>
							</thead>
							<tbody>
								{transactions.map(transaction => (
									<tr key={transaction._id}>
										<td>{transaction._id}</td>
										<td>
											{
												transaction.statusId === "5e9d2e50e6e19814785be47d" ?
												"Pending" : (transaction.statusId === "5e9d2e9ae6e19814785be47e" ?
												"Completed" : "Cancelled"
												)
											}
										</td>
										<td>{transaction.total}</td>
										<td>{transaction.sched}</td>
										{
											transaction.statusId === "5e9d2e50e6e19814785be47d" ?
											<td>

												<button 
													className="btn btn-success mr-2" 
													onClick={() => updateStatus("5e9d2e9ae6e19814785be47e", transaction._id)}
												>
													Complete
												</button>
												<button 
													className="btn btn-danger" 
													onClick={() => updateStatus("5e9d2ea1e6e19814785be47f", transaction._id)}
												>
													Cancel
												</button>
											</td> : null
										}
									</tr>
								))}
							</tbody>
						</table> :
						<h2 className="notrans">No Transactions to show</h2>
					}
				</div>
			</div>
		</Fragment>
	)
}

export default Transactions