import React from "react"
import "./ContactUs.css"

const ContactUs = () => {
	return (
		<div  className="contactus">
			<div className="container">
				<div className="contact-box">
					<div className="left"></div>
					<div className="right">
						<h2>Contact Us</h2>
						<input type="text" name="name" className="field" placeholder="Your Name"/>
						<input type="email" name="name" className="field" placeholder="Your Email"/>
						<input type="text" name="name" className="field" placeholder="Your Phone Number"/>
						<textarea className="field area" placeholder="Message"></textarea>
						<button className="btn btn-success">SEND</button>
					</div>
				</div>		
			</div>
		</div>
	)
}

export default ContactUs